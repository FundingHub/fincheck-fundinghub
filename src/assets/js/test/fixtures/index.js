export const funders = [
  {
    "id":"974!3!7",
    "product":"Invoice Discounting",
    "logoUrl":"https://saturn.xpertekhost.co.za/XFHLogos/CapX.png",
    "provider":"CapX Finance",
    "rating":"100%"
  },
  {
    "id":"974!4!4",
    "product":"Merchant Finance",
    "logoUrl":"https://saturn.xpertekhost.co.za/XFHLogos/Mettle.JPG",
    "provider":"Mettle Merchant Finance",
    "rating":"98%"
  },
  {
    "id":"974!3!3",
    "product":"Invoice Discounting",
    "logoUrl":"https://saturn.xpertekhost.co.za/XFHLogos/COMBINED.JPG",
    "provider":"Combined Finance",
    "rating":"98%"
  },
  {
    "id":"974!9!9",
    "product":"Business Cash Advance",
    "logoUrl":"https://saturn.xpertekhost.co.za/XFHLogos/Lulalend.jpg",
    "provider":"Lulalend (Pty) Ltd",
    "rating":"96%"
  },
  {
    "id":"974!4!8",
    "product":"Gap Access",
    "logoUrl":"https://saturn.xpertekhost.co.za/XFHLogos/NEDBANK.jpg",
    "provider":"Nedbank Limited",
    "rating":"93%"
  },
  {
    "id":"974!3!8",
    "product":"Cash Solutions/Invoice Discounting",
    "logoUrl":"https://saturn.xpertekhost.co.za/XFHLogos/NEDBANK.jpg",
    "provider":"Nedbank Limited",
    "rating":"93%"
  },
  {
    "id":"974!5!8",
    "product":"Overdraft",
    "logoUrl":"https://saturn.xpertekhost.co.za/XFHLogos/NEDBANK.jpg",
    "provider":"Nedbank Limited",
    "rating":"93%"
  },
  {
    "id":"974!10!8",
    "product":"Credit Card",
    "logoUrl":"https://saturn.xpertekhost.co.za/XFHLogos/NEDBANK.jpg",
    "provider":"Nedbank Limited",
    "rating":"93%"
  },
  {
    "id":"974!9!8",
    "product":"Unsecured Loans",
    "logoUrl":"https://saturn.xpertekhost.co.za/XFHLogos/NEDBANK.jpg",
    "provider":"Nedbank Limited",
    "rating":"93%"
  },
];
