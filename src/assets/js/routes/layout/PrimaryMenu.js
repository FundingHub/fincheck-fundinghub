import React from 'react';
import {Link} from 'react-router-dom';

const PrimaryMenu = () => (
  <nav>
    <ul className="menu align-right">
      <li>
        <Link to="/" className="active">
          get funding
        </Link>
      </li>
      <li>
        <a href="https://www.business.fincheck.co.za/" className="finchcol">HOME</a>
      </li>
    </ul>
  </nav>
);

export default PrimaryMenu;
