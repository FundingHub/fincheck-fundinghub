import React from 'react';

const Logo = () => (


   
    <a className="logo" href="https://www.business.fincheck.co.za">
      <img src="/Fincheck/assets/img/logo.png" alt="FinCheck in partnership with FundingHub" />
    </a>
   
);

export default Logo;
