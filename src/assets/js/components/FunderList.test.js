import React from 'react';
import {shallow} from 'enzyme';
import {spy} from 'sinon';
import Checkbox from 'material-ui/Checkbox';

import * as fixtures from '../test/fixtures';
import FunderList from './FunderList';

describe('<FunderList />', () => {
  test('-> when rendered', () => {
    const input = {onChange: jest.fn()};
    const meta = {};
    const {funders} = fixtures;
    let subject = shallow(<FunderList input={input} funders={funders} meta={meta} />);

    expect(subject.find('.funder-list-item').length, 'renders all funders').toBe(funders.length);

    let firstFunderChk = subject.find(Checkbox).get(0);
    firstFunderChk.props.onChange(true);

    expect(input.onChange, 'onChange called in response to check').toHaveBeenCalled();
    expect(input.onChange, 'adds the new funder').toHaveBeenNthCalledWith(1, [funders[0].id]);

    input.onChange.mockReset();
    input.value = [funders[0].id];

    subject = shallow(<FunderList input={input} funders={funders} meta={meta} />);
    firstFunderChk = subject.find(Checkbox).get(0);
    firstFunderChk.props.onChange(false);

    expect(input.onChange, 'removes the existing funder').toHaveBeenNthCalledWith(1, []);

    // Check twice
    firstFunderChk.props.onChange(true);
    firstFunderChk.props.onChange(true);

    expect(input.onChange, 'does not add duplicate').toHaveBeenNthCalledWith(2, [funders[0].id]);
  });
});
