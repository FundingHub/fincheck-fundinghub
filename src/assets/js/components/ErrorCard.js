import PropTypes from 'prop-types';
import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
//import {colors} from '../constants';
function getError(error) {
  if (error.errors) {
    // eslint-disable-next-line no-underscore-dangle
    return error.errors._error;
  }
  return error;
}

const ErrorCard = ({error, onCloseClick}) => {
  const err = getError(error);

  return (
    <Card style={{backgroundImage: 'linear-gradient(0deg #FEFEFE #FEFEFE)'}}>
      <CardHeader
        title="Dear Applicant..."
        subtitle={err.name}
        avatar={
          <div className="soft--small--ends">
            <img width="500" src="/Fincheck/assets/img/logo.png" role="presentation" />
          </div>
        }
      />
      <CardContent style={{color: 'black',backgroundColor: '#FEFEFE'}}>{err.message || 'Unknown Error'}</CardContent>

      {onCloseClick ? (
        <CardActions style={{color: 'black',backgroundColor: '#FEFEFE'}}>
          <Button onClick={onCloseClick} variant="text" style={{color: 'white',backgroundColor: '#00AEEF'}}>
            Close
          </Button>
        </CardActions>
      ) : null}
    </Card>
  );
};

ErrorCard.propTypes = {
  error: PropTypes.object.isRequired,
  onCloseClick: PropTypes.func,
};

export default ErrorCard;
