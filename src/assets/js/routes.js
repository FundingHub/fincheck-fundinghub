export default {
  aboutBusiness: '/',
  fundingRequirements: '/funding-requirements',
  trade: '/how-you-trade',
  funder: '/choose-a-funder',
  success: '/success',
};
