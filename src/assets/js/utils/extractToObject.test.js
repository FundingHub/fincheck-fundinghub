import extractToObject from './extractToObject';

describe('extractToObject', () => {
  test('-> returns values with empty strings', () => {
    const item = {key: 'newKey', value: ''};
    const array = [item];
    const result = extractToObject('key', 'value', array);

    expect(result).toHaveProperty('newKey', '');
  });

  test(`-> doesn't return values when undefined`, () => {
    const item = {key: 'newKey', value: undefined};
    const array = [item];
    const result = extractToObject('key', 'value', array);

    expect(result).not.toHaveProperty('newKey');
  });
});
