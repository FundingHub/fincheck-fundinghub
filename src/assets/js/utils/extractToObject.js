import assign from 'object.assign';

export default function extractToObject(keyKey, valueKey, array) {
  return array
    .filter(f => f[valueKey] !== undefined)
    .reduce((acc, f) => assign(acc, {[f[keyKey]]: f[valueKey]}), {});
}
