export default {
  
  finchgoogle: process.env.NODE_ENV === 'production' ? 'UA-118116860-1' : 'fake-ga-ua-code',
  googletag: process.env.NODE_ENV === 'production' ? 'GTM-K6WSZMP' : 'GTM-XXXX',
};
