import {createMuiTheme} from '@material-ui/core/styles';
import {redA100} from '@material-ui/core/colors';

const blue = '#00bcd4';
const green = '#00AEEF';
const grey = '#cacaca';
const greenNeon = '$4ad6ae';

const theme = createMuiTheme({
  palette: {
    primary: {main: green, contrastText: 'white'},
    secondary: {main: blue},
  },

  error: redA100,

  overrides: {
    MuiButton: {
      root: {
        backgroundColor: green,
        // color: 'white',
      },
      contained: {
        backgroundColor: green,
        color: 'white',
      },
      text: {
        backgroundColor: 'white',
        color: 'black',
      },
    },
    
    MuiCheckbox: {
      checked: {
        color: green,
      },
    },
    
    MuiCard: {
      checked: {
        color: 'white',
      },
    },

    MuiCardHeader: {
       root: {
        backgroundColor: '#EAE9E9',
        color: 'white',
       }
    },

    MuiStepIcon: {
      root: {
        color: grey,
        '&$active': {
          color: '#00AEEF',
        },
        '&$completed': {
          color: '#00AEEF',
        },
      },
    },
  },

  typography: {
    useNextVariants: true,
  },
});

export {green, greenNeon, grey};

export default theme;
