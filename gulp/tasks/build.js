const gulp = require('gulp');
const runSequence = require('run-sequence');

/*------------------------------------*\
     BUILD
\*------------------------------------*/
gulp.task('build', done => {
  process.env.NODE_ENV = 'production';

  return runSequence(
    'clean:build',
    ['copy', 'scripts:minify', 'css:minify', 'favicons:copy'],
    'useref',
    done,
  );
});
