const gulp    = require('gulp');
const extend  = require('extend');
const ftp     = require('vinyl-ftp');
const globule = require('globule');
const gutil   = require('gulp-util');

const conf    = require('../gulpconfig');
const secret  = require('../secrets');

const processFTP = function(done, ftpOpts, conOpts, dest) {
  const ftpConn = ftp.create(extend({
    parrallel: 10,
    log: gutil.log
  }, conOpts));

  gutil.log(gutil.colors.green("FTPing to " + ftpOpts.dest));

  return gulp.src(globule.find(ftpOpts.files), {
    base: ftpOpts.base,
    buffer: false
  }).pipe(ftpConn.newer(ftpOpts.dest)).pipe(ftpConn.dest(ftpOpts.dest));
};

const prepareFTP = function(done, direction, opts = {}) {
  const env = direction === 'up' ? 'prod' : 'dev';
  const ftpConf = conf.ftp[direction];
  const ftpSecrets = secret.ftp[env];
  const connOpts = {
    host: ftpSecrets.host,
    user: ftpSecrets.user,
    password: ftpSecrets.password
  };
  const ftpOpts = {
    dest: ftpConf.dest,
    base: ftpConf.base,
    files: conf.ftp[direction].files
  };

  return processFTP(done, ftpOpts, connOpts);
};

gulp.task('ftp:up', function(done) {
  return prepareFTP(done, 'up');
});

gulp.task('ftp:down', function(done) {
  return prepareFTP(done, 'down');
});
