const gulp    = require('gulp');
const eslint  = require('gulp-eslint');
const webpack = require('webpack');

const path = require('../gulpconfig').path;

function getError(stats) {
  if (stats && stats.compilation && stats.compilation.errors) {
    return stats.compilation.errors[0];
  }
  return null;
}

function runWebPack(config, done) {
  webpack(config, function(err, stats) {
    const error = err || getError(stats);
    if (error) {
      return done(error);
    }

    console.log(stats.toString({ chunks: false }));
    return done();
  })
}





/*------------------------------------*\
     SCRIPTS
\*------------------------------------*/
gulp.task('scripts:minify', function(done) {
  let webpackConf = require('../../webpack.config.prod');

  return runWebPack(webpackConf, done);
});





/*------------------------------------*\
     SCRIPTS WATCH
\*------------------------------------*/
gulp.task('scripts:watch', ['scripts'],  () => global.browserSync.reload());





/*------------------------------------*\
     SCRIPTS VENDORS
\*------------------------------------*/
gulp.task('scripts:vendors', function(done) {});
  // files = [
  //   "#{path.dev.js}/vendor.js"
  // ]

  // runWebPack(entries, {}, done)
  //





/*------------------------------------*\
     SCRIPTS LINTING
\*------------------------------------*/
gulp.task('scripts:lint', function() {
  let files = [
    `${path.dev.js}/**/*.js`,
  ];

  return gulp.src(files)
    .pipe(eslint())
    .pipe(eslint.format());
});
