const {NODE_ENV: env} = process.env;
function handleError(err) {
  if (env === 'production') {
    throw err;
  }
  console.log(err.toString());
  return this.emit('end');
}

module.exports = {
  handleError,
};
