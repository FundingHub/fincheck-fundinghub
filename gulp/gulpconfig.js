const devPath = 'src';
const devAssets = `${devPath}/assets`;
const distPath = 'dist';
const distAssets = `${distPath}/assets`;
const pugData = require('./pug');

const config = {
  path: {
    dev: {
      app: devPath,
      assets: `${devAssets}`,
      css:    `${devAssets}/css`,
      js:     `${devAssets}/js`,
      img:    `${devAssets}/img`,
      fnt:    `${devAssets}/fnt`
    },
    dist: {
      app: distPath,
      assets: `${distAssets}`,
      css:    `${distAssets}/css`,
      js:     `${distAssets}/js`,
      img:    `${distAssets}/img`,
      fnt:    `${distAssets}/fnt`
    }
  },

  pug: pugData,

  sass: {
    includePaths: ['node_modules/foundation-sites/scss'],
  },

  ftp: {
    up: {
      base: './dist',
      dest: "./public_html/Fincheck",
      files: [
        "./dist/**/*",
        "./dist/.htaccess",
        "!*/.git*",
        "!*/.DS_Store",
        "!node_modules",
        "!**/.sass-cache",
        "!/public_html/assets/js/app.bundle.js",
        // "!/**/*.ttf",
      ]
    },
    down: {
      base: './public_html',
      dest: "./localtest",
      files: "./test/**"
    }
  }
};

module.exports = config;
