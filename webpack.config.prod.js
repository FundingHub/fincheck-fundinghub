const path = require('path');
const webpack = require('webpack');

const conf = require('./gulp/gulpconfig');
const webpackBase = require('./webpack.config.base');

module.exports = {
  entry: {
    app: './src/assets/js/index.js',
  },

  output: webpackBase.output,

  externals: webpackBase.externals,

  resolve: webpackBase.resolve,

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
    ],
  },

  devtool: 'source-map',

  plugins: [].concat.apply(
    [
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production'),
        },
        'webpack.apiBaseUrl': JSON.stringify(
         // 'https://saturn.xpertekhost.co.za/scripts/fundingTest.dll',
           'https://saturn.xpertekhost.co.za/scripts/fundingHub.dll',
        ),
      }),
    ],
    webpackBase.plugins,
  ),

  optimization: {
    minimize: true,
  },
};
