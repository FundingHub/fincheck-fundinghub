const express    = require('express');        // call express
const app        = express();                 // define our app using express
const bodyParser = require('body-parser');
const morgan     = require('morgan');
const cors       = require('cors');

const routes     = require('./routes');

app.use(cors());

app.use(morgan('combined'));
app.use(express.static('public'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const port = process.env.PORT || 8080;        // set our port


app.use('/api', routes);

app.listen(port, () => {
  console.log('Running on port ' + port);
});
