const express = require('express');        // call express
const path = require('path');
const fs = require('fs');

const router = express.Router();              // get an instance of the express Router

router.get('/', (req, res) => {
  res.json({ message: 'hooray! welcome to our api!' });
});

function serveJSON({ path: endpoint, file, method }) {
  router[method](endpoint, (req, res) => {
    res.json(JSON.parse(fs.readFileSync(path.resolve(__dirname, file))))
  });
}

serveJSON({ path: '/api1', file: './public/api1.json', method: 'post' });
serveJSON({ path: '/api2', file: './public/api2.json', method: 'post' });
serveJSON({ path: '/api3', file: './public/api3.json', method: 'post' });
serveJSON({ path: '/error', file: './public/error.json', method: 'post' });

module.exports = router;
