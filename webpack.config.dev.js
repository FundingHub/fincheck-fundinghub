const path = require('path');
const webpack = require('webpack');

const conf = require('./gulp/gulpconfig');
const webpackBase = require('./webpack.config.base');

module.exports = {
  mode: 'development',

  entry: {
    app: ['webpack/hot/dev-server', 'webpack-hot-middleware/client', './src/assets/js/index.js'],
  },

  output: {
    path: path.join(__dirname, '../', conf.path.dist.js),
    publicPath: webpackBase.output.publicPath,
    filename: webpackBase.output.filename,
  },

  externals: webpackBase.externals,

  resolve: webpackBase.resolve,

  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: ['react-hot-loader/webpack', 'babel-loader'],
      },
    ],
  },

  plugins: [].concat.apply(
    [
      new webpack.HotModuleReplacementPlugin(),
      // new webpack.NoErrorsPlugin(),
      new webpack.DefinePlugin({
        'webpack.apiBaseUrl': JSON.stringify(
          //'https://saturn.xpertekhost.co.za/scripts/fundingTest.dll',
           'https://saturn.xpertekhost.co.za/scripts/fundingHub.dll',
        ),
      }),
    ],
    webpackBase.plugins,
  ),

  devServer: {
    contentBase: webpackBase.output.publicPath,
  },
};
